#!/usr/bin/env python3

import decode as d
import encode as e
import matplotlib.pyplot as plt

from math import sin

# button binary_data
# 1off   101100010010101101101111
# 1on    101110000001010101001111
# 2off   101101010111111101011011
# 2on    101101111000011010001011
# 3off   101110110101000011010011
# 3on    101110011010011110110011
# 4off   101100101100111011001101
# 4on    101101001101010011101101
# alloff 101110011010011110110101
# allon  101100010010101101100101

def analyze(debug=0):
    files = [
    "gt-9000_gumb_1off_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_1on_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_2off_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_2on_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_3off_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_3on_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_4off_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_4on_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_alloff_10dB_433000000Hz_2048000sps.offset8iq",
    # "gt-9000_gumb_allon_10dB_433000000Hz_2048000sps.offset8iq",
    ]

    for filename in files:
        print(filename)
        frames_i, frames_q = d.trigger_from_file_u8("./pysdrsde-data/gt-9000/" + filename,
            easbt=4000, trigger_level=10, byte_decode_f=d.value_shift_decode_8bit, stop_after=None)

        for frame in frames_i:
            try:
                d.pwm_decode(frame, debug, pop=0)
            except:
                pass

def encode(bin_string, filename, debug=0):
    """
    if filename == "-" then print to stdout
    """

    bin_data = [False if c == "0" else True for c in bin_string]
    signal_data = e.bin_data_to_bpwm_logic_signal(bin_data, short_len=800, long_len=2300)

    pulse1 = [1 for i in range(800)]
    pause1 = [0 for i in range(4500)]

    pulse2 = [1 for i in range(6500)]
    pause2 = [0 for i in range(14000)]

    # signal =    pulse1 + pause1 + \
    #             signal1 + pause1 + \
    #             signal1 + pause1 + \
    #             signal2 + pulse2 + pause2 +  \
    #             signal2 + pulse2 + pause2 +  \
    #             signal2 + pulse2 + pause2 +  \
    #             signal1
    signal = pulse1 + pause1 + \
                signal_data + pulse1 + pause1 + \
                signal_data + pulse1 + pause1 + \
                signal_data + pulse1 + pause1 + \
                signal_data + pulse2 + pause2 +  \
                signal_data + pulse2 + pause2 +  \
                signal_data + pulse2 + pause2 +  \
                signal_data + pulse2 + pause2 +  \
                signal_data

    signal = [int(120*val*sin(i/10)) for i, val in enumerate(signal)]
    # signal = [int(120*val) for i, val in enumerate(signal)]

    if debug > 0:
        plt.plot(signal)
        plt.show()

    if filename == "-":
        # write to stdout
        e.arr_to_stdout(signal, signal, byte_encode_f=e.twos_complement_encode_8bit)
    else:
        # write to file
        e.arr_to_file(signal, signal, filename, byte_encode_f=e.twos_complement_encode_8bit)



def button(num, state, filename, debug=0):
    """
    if num == 0 set all buttons to "state"
    otherwise set "num" button to "state"
    """

    if num == 0:
        if state == False:
            # button alloff
            encode("101110011010011110110101", filename, debug=debug)
        elif state == True:
            # button allon
            encode("101100010010101101100101", filename, debug=debug)
    elif num == 1:
        if state == False:
            # button 1off
            encode("101100010010101101101111", filename, debug=debug)
        elif state == True:
            # button 1on
            encode("101110000001010101001111", filename, debug=debug)
    elif num == 2:
        if state == False:
            # button 2off
            encode("101101010111111101011011", filename, debug=debug)
        elif state == True:
            # button 2on
            encode("101101111000011010001011", filename, debug=debug)
    elif num == 3:
        if state == False:
            # button 3off
            encode("101110110101000011010011", filename, debug=debug)
        elif state == True:
            # button 3on
            encode("101110011010011110110011", filename, debug=debug)
    elif num == 4:
        if state == False:
            # button 4off
            encode("101100101100111011001101", filename, debug=debug)
        elif state == True:
            # button 4on
            encode("101101001101010011101101", filename, debug=debug)

if __name__ == "__main__":

    analyze(debug=1)

    # button(2, 0, "/dev/null", debug=1)
    # button(2, 1, "-", debug=0)
    # button(2, 0, "-", debug=0)

