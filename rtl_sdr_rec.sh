#!/bin/sh

if [ "$#" -ne 4 ]; then
    echo "Wrong number of parameters"
    exit
fi

name=$1
gain=$2
freq=$3
samplerate=$4

FILENAME=${name}_${gain}dB_${freq}Hz_${samplerate}sps.s8

if [ -f $FILENAME ]; then
    echo "file exists: $FILENAME"
    exit
fi

rtl_sdr -f $freq -g $gain -s $samplerate - > "$FILENAME"

