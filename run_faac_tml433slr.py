#!/usr/bin/env python3

import decode as d
import encode as e
import matplotlib.pyplot as plt

# manchester
i_data, q_data = d.read_file_u8("./pysdrsde-data/faac_tml433slr_20dB_433500000Hz_2048000sps.offset8iq")
frames = d.trigger(i_data, 4000, 20)
# for frame in frames:
#     plt.plot(frame)
#     plt.show()
d.manchester_decode(frames[2], debug=0)

