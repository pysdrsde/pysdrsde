#!/usr/bin/env python3

import sys

def twos_complement_encode_8bit(n):
    if 0 <= n < 128:
        return n
    elif -128 <= n < 0:
        return 256 + n
    else:
        print("error: " + str(n))
        return 0

def value_shift_encode_8bit(n):
    return n + 128

def arr_to_file(arr_i, arr_q, filename, byte_encode_f):
    bytes_per_sample = 2
    samples_per_chunk = 256
    chunk_size = bytes_per_sample * samples_per_chunk
    chunk = []

    with open(filename, 'bx') as bf:
        for i,q in zip(arr_i, arr_q):
            # convert to apropriate format
            chunk.append(byte_encode_f(i))
            chunk.append(byte_encode_f(q))
            if len(chunk) >= chunk_size:
                bf.write(bytes(chunk))
                chunk = []
        # write the remaining < 256 bytes
        if len(chunk) != 0:
            bf.write(bytes(chunk))
            chunk = []

def arr_to_stdout(arr_i, arr_q, byte_encode_f):
    bytes_per_sample = 2
    samples_per_chunk = 256
    chunk_size = bytes_per_sample * samples_per_chunk
    chunk = []

    for i,q in zip(arr_i, arr_q):
        # convert to apropriate format
        chunk.append(byte_encode_f(i))
        chunk.append(byte_encode_f(q))
        if len(chunk) >= chunk_size:
            sys.stdout.buffer.write(bytes(chunk))
            chunk = []
    # write the remaining < 256 bytes
    if len(chunk) != 0:
        sys.stdout.buffer.write(bytes(chunk))
        chunk = []

def bin_data_to_bpwm_logic_signal(bin_data, short_len, long_len, polarity=True):
    out = []
    for bit in bin_data:
        if bit == polarity:
            out.extend([True for i in range(short_len)])
            out.extend([False for i in range(long_len)])
        else:
            out.extend([True for i in range(long_len)])
            out.extend([False for i in range(short_len)])

    return out

