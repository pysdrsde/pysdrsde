#!/usr/bin/env python3

import decode as d
import encode as e
import matplotlib.pyplot as plt

# PWM encoding (25% in 75%)
i_data, q_data = d.read_file_u8("./pysdrsde-data/bel_12gumbov_20dB_433500000Hz_2048000sps.offset8iq")
frames = d.trigger(i_data, 4000, 20)
# for frame in frames:
#     plt.plot(frame)
#     plt.show()
d.pwm_decode(frames[0], debug=0)

