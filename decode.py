#!/usr/bin/env python3

import matplotlib.pyplot as plt
from math import sqrt, atan2, sin, cos
from scipy.signal import butter,lfilter,freqz
import sys
import struct
import os
import numpy as np

def twos_complement_decode_8bit(n):
    if 0 <= n < 128:
        return n
    elif 128 <= n < 256:
        return n - 256
    else:
        print("error: " + str(n))
        return 0

def value_shift_decode_8bit(n):
    return n - 128

def read_file_u8(filename, start=0, end=None):
    bytes_per_sample = 2

    file_size = os.path.getsize(filename)

    if (end == None) or (end > file_size):
        end = file_size//bytes_per_sample

    delta = end - start
    i_data = np.zeros(delta)
    q_data = np.zeros(delta)

    with open(filename, 'rb') as bf:
        if start != 0:
            # convert 'start' from samples to bytes
            pos = start*bytes_per_sample
            bf.seek(pos, os.SEEK_SET)

        # if file is big this will consume lots of ram
        buff = bf.read(delta * bytes_per_sample)

        for i in range(delta):
            i_data[i] = buff[2*i] - 128
            q_data[i] = buff[2*i+1] - 128

        # plt.plot(i_data)
        # plt.plot(q_data)
        # plt.show()

        return i_data, q_data

def trigger_from_file_u8(filename, easbt, trigger_level,
        byte_decode_f, stop_after=None):
    """
    "easbt" - end after samples below trigger_level

    Reads the file and splits it into frames.
    Frame starts when level is above "trigger_level".
    Frame ends when signal is below trigger level for
    more than "easbt".

    if "stop_after" is not "None" then the search stops
    after "stop_after" frames are found
    """

    curr_sample = []
    bytes_per_sample = 2
    samples_per_chunk = 256

    i_data = [None for _ in range(samples_per_chunk)]
    q_data = [None for _ in range(samples_per_chunk)]

    frames_i = []
    frames_q = []
    curr_frame_i = []
    curr_frame_q = []

    in_frame = False
    # current samples below trigger_level
    curr_sbt = 0

    with open(filename, 'rb') as bf:
        while True:
            chunk = bf.read(bytes_per_sample * samples_per_chunk)

            if not chunk:
                # end of file reached
                if in_frame == True:
                    frames_i.append(curr_frame_i)
                    frames_q.append(curr_frame_q)
                break

            for i in range(len(chunk)//bytes_per_sample):
                i_data[i] = byte_decode_f(chunk[2*i])
                q_data[i] = byte_decode_f(chunk[2*i+1])
            data = [abs(i) + abs(q) for i, q in zip(i_data, q_data)]

            if in_frame == False:
                # no frame detected yet

                for i, sample in enumerate(data):
                    if sample > trigger_level:
                        in_frame = True
                        curr_sbt = 0
                        curr_frame_i = i_data[:]
                        curr_frame_q = q_data[:]
                        # curr_frame_i = i_data[i:]
                        # curr_frame_q = q_data[i:]
                        break

            elif in_frame == True:
                # we are inside frame

                for i, sample in enumerate(data):
                    if sample > trigger_level:
                        curr_sbt = 0
                    else:
                        curr_sbt += 1
                        if curr_sbt > easbt:
                            # end of frame
                            in_frame = False
                            curr_frame_i.extend(i_data[:i])
                            curr_frame_q.extend(q_data[:i])
                            frames_i.append(curr_frame_i)
                            frames_q.append(curr_frame_q)
                            if stop_after != None:
                                stop_after -= 1
                            print(len(curr_frame_i))
                            break

                curr_frame_i.extend(i_data[:])
                curr_frame_q.extend(q_data[:])

            if stop_after != None and stop_after == 0:
                break
        return frames_i, frames_q

def trigger(arr, samples_per_symbol, trigger_level):
    samples = []

    curr_sample = []

    buf_len = samples_per_symbol
    prev_buf = []
    for i in range(len(arr) // buf_len):
        buf = arr[i*buf_len : (i+1)*buf_len]

        # detect beginning of frame
        if np.max(np.abs(buf)) > trigger_level:
            # prepend previous buf so we have
            # some noise before the actual frame
            if len(curr_sample) == 0:
                curr_sample.extend(prev_buf)

            curr_sample.extend(buf)

        # detected end of frame
        elif len(curr_sample) > 0:

            # also save some noise at the end
            curr_sample.extend(buf)

            samples.append(curr_sample)
            curr_sample = []

        prev_buf = buf

    # for s in samples[:3]:
    #     plt.plot(s)
    #     plt.show()
    return samples


def low_pass_filter(arr):
    b, a = butter(3, 0.01, btype='low', analog=False)
    return lfilter(b, a, arr)



def amplitude_comparator(arr, threshold):
    digital = np.zeros(len(arr))

    for i, val in enumerate(arr):
        if val >= threshold:
            digital[i] = True
        else:
            digital[i] = False

    return digital

def count_consecutive(logic_arr, debug=0):
    ret = list([0])
    i = 0
    prev_val = logic_arr[i]

    for curr_val in logic_arr:
        if curr_val == prev_val:
            ret[-1] += 1
        else:
            prev_val = curr_val
            ret.append(1)

    if debug > 0:
        print('consecutive pulse lengths:')
        print(ret)
    return ret


def frequency_of_values(arr, mult_factor=1.4, debug=0):
    arr_min = np.min(arr)
    arr_max = np.max(arr)

    if debug > 0:
        print("min: ", arr_min)
        print("max: ", arr_max)

    p2p = arr_max - arr_min + 1

    log_mult_factor = np.log(mult_factor)

    divisions = int(np.ceil(np.log(p2p) / log_mult_factor))
    if debug > 0:
        print("divisions: ", divisions)

    val_count = np.zeros(divisions)

    for i in arr:
        val_count[int(np.log(i - arr_min + 1) / log_mult_factor)] += 1

    x = [arr_min + mult_factor**i for i in range(divisions)]

    if debug > 0:
        # plt.plot(x, np.log(val_count), '.')
        plt.plot(x, val_count, '.')
        plt.title('Frequency of values')
        plt.xlabel('value')
        plt.ylabel('number of occurances')
        plt.show()

    return val_count, x


def manchester_decode(arr, debug=0):
    arr = np.abs(arr)
    raw = arr

    arr = low_pass_filter(arr)
    filtered = arr

    y, x = frequency_of_values(arr, mult_factor=1.4)
    most = np.max(y)

    # # find first maximum
    # i = 0
    # while y[i] != most:
    #     i += 1
    # # find first valley
    # while y[i] > y[i+1]:
    #     i += 1
    # threshold = x[i]
    threshold = (np.min(arr) + np.max(arr)) / 2
    if debug > 0:
        print("threshold: ", threshold)

    arr = amplitude_comparator(arr, threshold)
    binary = arr

    if debug > 0:
        plt.plot(raw/np.max(raw))
        plt.plot(filtered/np.max(filtered))
        plt.plot(binary/np.max(binary))
        plt.show()

    pulses = count_consecutive(arr)

    # remove first and last "pulse"
    pulses = pulses[1:-1]
    # pulses[0] is now the length of the first positive (True) pulse

    positive_pulses = pulses[::2]
    negative_pulses = pulses[1::2]
    # frequency_of_values(positive_pulses, mult_factor=1.4)
    # frequency_of_values(negative_pulses, mult_factor=1.4)

    short_pulse = np.min(negative_pulses)
    long_pulse = np.max(negative_pulses)
    middle = (short_pulse + long_pulse) / 2
    if debug > 0:
        print("short_pulse: ", short_pulse)
        print("long_pulse: ", long_pulse)
        print("middle: ", middle)

    bin_data = []

    i = 0
    # skip first pulse
    # if pulses[i] >= middle:
    #     print("error: first pulse too long")
    i += 1

    curr_val = False
    while i < len(pulses):
        # one long pulse
        if pulses[i] >= middle:
            curr_val = not curr_val
            bin_data.append(curr_val)
            i += 1

        # two short pulses
        elif pulses[i] < middle:
            curr_val = not curr_val
            i += 1
            if pulses[i] >= middle:
                print("error: unexpected long pulse")
                curr_val = not curr_val
                i += 1
            elif pulses[i] < middle:
                curr_val = not curr_val
                bin_data.append(curr_val)
                i += 1

    s = ''.join(["0" if i == False else "1" for i in bin_data])
    print(s)
    return bin_data


def pwm_decode(arr, debug=0, pop=0):
    arr = np.abs(arr)
    raw = arr

    arr = low_pass_filter(arr)
    filtered = arr

    y, x = frequency_of_values(arr, mult_factor=1.4)
    most = np.max(y)

    threshold = (np.min(arr) + np.max(arr)) / 2
    if debug > 0:
        print("threshold: ", threshold)

    arr = amplitude_comparator(arr, threshold)
    binary = arr

    if debug > 0:
        plt.plot(raw/np.max(raw))
        plt.plot(filtered/np.max(filtered))
        plt.plot(binary/np.max(binary))
        plt.legend(['usmerjen signal', 'zglajen signal', 'binarni signal'], loc='lower right')
        # plt.title('Decoded frame')
        plt.show()

    pulses = count_consecutive(arr)

    # remove <pop> pulses from beginning - remove preamble
    for i in range(pop):
        pulses.pop(0)

    # remove first and last "pulse"
    pulses = pulses[1:-1]
    # pulses[0] is now the length of the first positive (True) pulse

    positive_pulses = pulses[::2]
    negative_pulses = pulses[1::2]
    # frequency_of_values(positive_pulses, mult_factor=1.4)
    # frequency_of_values(negative_pulses, mult_factor=1.4)

    short_pulse = np.min(negative_pulses)
    long_pulse = np.max(negative_pulses)
    middle = (short_pulse + long_pulse) / 2
    if debug > 0:
        print("short_pulse: ", short_pulse)
        print("long_pulse: ", long_pulse)
        print("middle: ", middle)


    bin_data = []

    i = 0
    # skip first pulse
    # if pulses[i] >= middle:
    #     print("error: first pulse too long")
    i += 1

    curr_val = False
    for pulse in positive_pulses:
        if pulse < middle:
            bin_data.append(1)
        else:
            bin_data.append(0)

    s = ''.join(["0" if i == False else "1" for i in bin_data])
    print(s)
    return s

