#!/usr/bin/env python3

import decode as d
import encode as e
import matplotlib.pyplot as plt

# PWM encoding (ramerje high/low), le da 33% in 66% duty cycle
i_data, q_data = d.read_file_u8("./pysdrsde-data/faac_zeleni_trikotnik_20dB_433500000Hz_2048000sps.offset8iq")
frames = d.trigger(i_data, 2000, 20)
# for frame in frames:
#     plt.plot(frame)
#     plt.show()
d.pwm_decode(frames[0], debug=0, pop=1)

