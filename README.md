
pysdrsde - PYthon Software Defined Radio Signal Decoding and Encoding

## Setup

first clone this repo (pysdrsde)

`$ git clone "https://gitlab.com/pysdrsde/pysdrsde.git"`

and change directory to it

`$ cd pysdrsde`

Then clone pysdrsde-data repository

`git clone "https://gitlab.com/pysdrsde/pysdrsde-data.git"`

